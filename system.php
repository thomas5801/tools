<?php 
include 'config.php';

    if(cek_session($url."api/v1/check") === false){
        header('Location: logout.php');
        exit;
    }
    
?>
<style>

.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #4CAF50;
}

input:focus + .slider {
  box-shadow: 0 0 1px #4CAF50;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}


ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
  width:900;
}

li {
  float: left;
   width:75;
}



li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #ddd;
  color: black;
}

.active {
  background-color: #4CAF50;
  color: white;;
}

.block {
  display: block;
  width: 100%;
  border: true;
  background-color: #4CAF50;
  color: white;
  padding: 3px 60px;
  font-size: 12px;
  cursor: pointer;
  text-align: center;
}

.block:hover {
  background-color: #c0bc03;
  color: black;
}

</style>





<?php

$menu = str_replace("{{active_system}}","active",$menu);
echo $menu;

?>



<br>

<table border="0" width='900'>
    <tr>
        <td> <h3>Auto Delete Event Participant</h3> </td>
    <tr>
    
</table>



<br>


<form action="system.php" method="get">
	<label>Cari Berdasarkan <b>Nama Event</b> :</label>
	<input type="text" name="cari">
	<input type="submit" value="Cari">
</form>

<?php

    if(isset($_GET['cari'])){
        $cari = $_GET['cari'];
        echo "<b>Hasil pencarian : ".$cari."</b>";
    }


    //error_reporting(0);

    // GET DATA
    $ch = curl_init(); 
    
    if(isset($cari)){
        $url_ = $url."/api/v1/autodelete?filter[evnhName][like]=%25".urlencode($cari)."%25"; 
    }else{
        $url_ = $url."/api/v1/autodelete"; 
    }


	// set url
	curl_setopt($ch, CURLOPT_URL, $url_);

	// return the transfer as a string 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

	// $output contains the output string 
	$output = curl_exec($ch); 

	// tutup curl 
	curl_close($ch);      

	// menampilkan hasil curl
	echo " \n ";

	echo " \n ";
    $data_all = json_decode($output);
	

	


?>




<table width='900' border="1">

    <tr>
        <th style='background-color:#c0bc03;' colspan="5" >=================== AUTO DELETE PARTICIPANT ===================</th>
    </tr>
    
	<tr>
        <th>No</th>
		<th>Event Name</th>
		<th>User Update</th>
		<th>Status</th>
		<th>Action</th>
    </tr>
    <?php 
        
            $x = 1;
            
            if(isset($data_all->data)){
                foreach($data_all->data as $vall ){
				
                    if($x % 2 == 0){
                         $style = "";
                    }else{
                         $style = "style='background-color:#ddd;'";
                    }
                   
				   $retry = 'onclick="window.location.href='."'".'update_auto_delete.php?id='.$vall->adsyId."&status=".$vall->adsyStatus."'".'"';
				   
				    if($vall->adsyStatus == '1'){
                        $toggle = "<label class='switch'>
						<input $retry type='checkbox' checked>
						<span class='slider round'></span>
						</label>";
						
						$vall->adsyStatus = "Active";
                    }else{
                        $toggle = "<label class='switch'>
						<input $retry type='checkbox' >
						<span class='slider round'></span>
						</label>";
						$vall->adsyStatus = "Inactive";
                    }
                    
                    
                    echo "<tr $style>";
					echo "<td><center>".$vall->evnhId."</center></td>";
					echo "<td>".$vall->evnhName."</td>";
					echo "<td>".$vall->user."</td>";
					echo "<td><b>".$vall->adsyStatus."<b></td>";
					echo "<td>".$toggle."</td>";	
                    echo "</tr>";
                 
					//echo "<tr $style>";
					//echo "<td colspan='5'>&nbsp;</td>";
                    //echo "</tr>";
                    
                    
                    
                    $x++;
                
                }
      
			
            }
        
    // <tr>
    //    <td><b>Event</b></td>
    //    <td>:</td>
    //    <td>dsadas</td>
    // </tr>
    
    ?>


    
</table>


