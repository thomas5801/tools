<?php 
include 'config.php';

    if(cek_session($url."api/v1/check") === false){
        header('Location: logout.php');
        exit;
    }

   
?>
<style>

.button {
  background-color: #4CAF50;
  border: none;
  color: white;
  padding: 3px 3px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 14px;
  margin: 1px 1px;
  cursor: pointer;
}

.button- {
  background-color: #ff0000;
  border: none;
  color: white;
  padding: 3px 3px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 14px;
  margin: 1px 1px;
  cursor: pointer;
}


.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #4CAF50;
}

input:focus + .slider {
  box-shadow: 0 0 1px #4CAF50;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}


ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
  width:900;
}

li {
  float: left;
   width:75;
}



li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #ddd;
  color: black;
}

.active {
  background-color: #4CAF50;
  color: white;;
}

.block {
  display: block;
  width: 100%;
  border: true;
  background-color: #4CAF50;
  color: white;
  padding: 3px 60px;
  font-size: 12px;
  cursor: pointer;
  text-align: center;
}

.block:hover {
  background-color: #c0bc03;
  color: black;
}

</style>


<?php

$menu = str_replace("{{active_agent}}","active",$menu);
echo $menu;

?>




<br>

<table border="0" width='900'>
    <tr>
        <td> <h3>Travel Agent Items Registration</h3> </td>
        <td align=right>
            <form action="submenu_agent.php" method="post">
                <label><b>Sub Menu : </b></label>
                <select onchange="this.form.submit()" name="action_agent" required>
                    <option value="items">Items Registration</option>
                    <option value="quota">Quota Registration</option>
                   
                </select>

            </form>
        </td>
    <tr>
    
</table>





<?php

    if(isset($_GET['response_error'])){
       echo "<br>";
       echo "<h3> ERROR :  <font color='red'>". urldecode($_GET['response_error'])."</font></h3>";
    }


?>


<br>



<form action="agent_items.php" method="get">
	<label>Event : </label>
	<select name="event" required>
      <option ></option>
        <option value="150">Borobudur Marathon 2019</option>
        </select>
    <label>Agent : </label>
	<input type="text" name="cari">
	<input type="submit" value="Cari">
</form>


<?php






     if(isset($_GET['event'])){
        $event = $_GET['event'];
    }
    
    if(isset($_GET['cari'])){
        $cari = $_GET['cari'];
        echo "<b>Hasil event : ".$event. "</b><br>";
        echo "<b>Hasil pencarian : ".$cari. "</b>";
    }


    if(isset($_GET['ok']) AND isset($_GET['plus'])){
        $ok = $_GET['ok'];
        $plus = "<font color='green'>(+".$_GET['plus'].")</font>";
   
    }

    if(isset($_GET['ok']) AND isset($_GET['minus'])){
        $ok = $_GET['ok'];
        $minus = "<font color='red'>(-".$_GET['minus'].")</font>";
   
    }

  


    

    //error_reporting(0);

    // GET DATA
    $ch = curl_init(); 
    
    if(isset($cari)){
        $url_ = $url."/api/v1/resources/agent_items?filter[evitUserName][like]=%25".urlencode($cari)."%25&filter[evitEvnhId]=".$event."&pageSize=1000"; 
    }else{
        $url_ = $url."/api/v1/resources/agent_items"; 
    }


	// set url
	curl_setopt($ch, CURLOPT_URL, $url_);

	// return the transfer as a string 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

	// $output contains the output string 
	$output = curl_exec($ch); 

	// tutup curl 
	curl_close($ch);      

	// menampilkan hasil curl
	echo " \n ";

	echo " \n ";
    $data_all = json_decode($output);



?>




<table width='900' border="1">

    <tr>
        <th style='background-color:#c0bc03;' colspan="9" >=================== Travel Agent Items ===================</th>
    </tr>
    
	<tr>
        <th>Agent</th>
        <th>Items Code</th>
        <th>Items Name</th>
		<th>Event ID</th>
		<th>Category</th>
		<th>Bobot</th>
        <th>Harga</th>
        <th>Quota</th>
		<th>Action</th>
    </tr>
    <?php 
        
            $x = 1;
            
            if(!isset($cari) OR !isset($event)){
                $cari="";
                $event="";
            }
            
            
            if(isset($data_all->data)){
                foreach($data_all->data as $vall ){
				
                    if($x % 2 == 0){
                         $style = "";
                    }else{
                         $style = "style='background-color:#ddd;'";
                    }
                     /*
                    foreach($data_all->linked->evitEvnhId as $vall_evnhId){
                        
                        if($vall->links->evitEvnhId == $vall_evnhId->evnhId){
                            $vall->links->evitEvnhId = $vall_evnhId->evnhName;
                            break;
                        }
                        
                    }
                    */
                   
                    foreach($data_all->linked->evitEvncId as $vall_evncId){
                        if($vall->links->evitEvncId == $vall_evncId->evncId){
                            $vall->links->evitEvncId = $vall_evncId->evncName;
                            break;
                        }
                    } 
                    
                    $id = $vall->id;
                    
                    echo "<tr $style>";
					echo "<td>".$vall->evitUserName."</td>";
                    echo "<td>".$vall->evitItems."</td>";
                    echo "<td>".$vall->evitName."</td>";
					echo "<td  align='center'>".$vall->links->evitEvnhId."</td>";
					echo "<td>".$vall->links->evitEvncId."</td>";
                    echo "<td  align='right'>".number_format($vall->evitAmount,0,",",".")."</td>";
                    echo "<td><font color='red'><b><center>".$vall->evitType."</center></b></td>";
                    
                    if(isset($ok) AND isset($plus) AND $ok == $vall->id){
                        echo "<td><center><b>".$vall->evitQuota.$plus."<b></center></td>";
                    }else if(isset($ok) AND isset($minus) AND $ok == $vall->id){
                        echo "<td><center><b>".$vall->evitQuota.$minus."<b></center></td>";
                    }else{
                        echo "<td><center><b>".$vall->evitQuota."<b></center></td>";
                    }
					
                    
                    echo "<td>
                        <center>
                        
                        <form action='update_agent_items.php' method='get'>
                        <input type='hidden' name='id' value='$id'>
                        <input type='hidden' name='cari' value='$cari'>
                        <input type='hidden'  name='event' value='$event'>
                        <input type='text'  size='3' name='tambah' required>
                        <input class='button' type='submit' value='(+)'>
                        </form>
                        
                        <form action='update_agent_items_minus.php' method='get'>
                        <input type='hidden' name='id' value='$id'>
                        <input type='hidden' name='cari' value='$cari'>
                        <input type='hidden'  name='event' value='$event'>
                        <input type='text'  size='3' name='kurang' required>
                        <input class='button-' type='submit' value='(-)'>
                        </form>
                        </center>
                        </td>";
                   

                    
                    $x++;
                
                }
      
			
            }
        

    ?>


    
</table>

<br>
<br>
<br>
<br>
<br>


