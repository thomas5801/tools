<?php 
include 'config.php';

    if(cek_session($url."api/v1/check") === false){
        header('Location: logout.php');
        exit;
    }

   
?>
<style>


.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #4CAF50;
}

input:focus + .slider {
  box-shadow: 0 0 1px #4CAF50;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}


ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
  width:900;
}

li {
  float: left;
   width:75;
}



li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #ddd;
  color: black;
}

.active {
  background-color: #4CAF50;
  color: white;;
}

.block {
  display: block;
  width: 100%;
  border: true;
  background-color: #4CAF50;
  color: white;
  padding: 3px 60px;
  font-size: 12px;
  cursor: pointer;
  text-align: center;
}

.block:hover {
  background-color: #c0bc03;
  color: black;
}

</style>

<?php

$menu = str_replace("{{active_remove}}","active",$menu);
echo $menu;

?>

<br>

<table border="0" width='900'>
    <tr>
        <td> <h3>Remove Items Transaction</h3> </td>
    <tr>
    
</table>



<br>


<form action="remove.php" method="get">
	<label>Cari Berdasarkan <b>RefID</b> : </label>
	<input type="text" name="cari">
	<input type="submit" value="Cari">
</form>

<?php

    if(isset($_GET['cari'])){
        $cari = $_GET['cari'];
        echo "<b>Hasil pencarian : ".$cari."</b>";
    }


    //error_reporting(0);

    // GET DATA
    $ch = curl_init(); 
    
    if(isset($cari)){
        $url_ = $url."/api/v1/transaction/$cari"; 
    }else{
        $url_ = $url."/api/v1/transaction/"; 
    }


	// set url
	curl_setopt($ch, CURLOPT_URL, $url_);

	// return the transfer as a string 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

	// $output contains the output string 
	$output = curl_exec($ch); 

	// tutup curl 
	curl_close($ch);      

	// menampilkan hasil curl
	echo " \n ";

	echo " \n ";
    

    $data_all = json_decode($output);
	

?>



<?php


if(isset($data_all->data)){
    
    echo "<br>";
    echo "<br>";
    echo "<br>";
    echo "<table width='900' border='0'>";


	echo "<tr>";
    echo "<td>Event Name</th>";    
	echo "<td> : </th>";
    echo "<td><b>".$data_all->linked->trnsEventId[0]->evnhName."</b></th>";
    echo "</tr>";

	echo "<tr>";
    echo "<td>Registration Code</th>";    
	echo "<td> : </th>";
    echo "<td><b>".$data_all->data->trnsRefId."</b></th>";
    echo "</tr>";
    
    
    echo "<tr>";
    echo "<td>Participant Name</th>";    
	echo "<td> : </th>";
    echo "<td><b>".$data_all->data->trnsUserName."</b></th>";
    echo "</tr>";
    
    echo "<tr>";
    echo "<td>Transaction Date</th>";    
	echo "<td> : </th>";
    echo "<td><b>".$data_all->data->trnsCreatedTime."</b></th>";
    echo "</tr>";
    
    
    echo "<tr>";
    echo "<td>Final Amount</th>";    
	echo "<td> : </th>";
    echo "<td><b>".$data_all->data->trnsAmount."</b></th>";
    echo "</tr>";


    echo "<tr>";
    echo "<td>Status</th>";    
	echo "<td> : </th>";
    
    if(isset($data_all->data->trnsConfirmed) AND $data_all->data->trnsConfirmed == 1){
        
        echo "<td><b>Confirmed</b></th>";
    }else{
        echo "<td><b>Pending</b></th>";
    }
    
    echo "</tr>";



   
   
    echo "</table>";

    echo "<br>";
}




?>


<table width='900' border="1">

    <tr>
        <th style='background-color:#c0bc03;' colspan="5" >=================== REMOVED ITEMS ===================</th>
    </tr>
    
	<tr>
        <th>No</th>
		<th>Description</th>
        <th>Amount</th>
		<th>Remove</th>
    </tr>
    <?php 
        
            $x = 1;
            
            if(isset($data_all->data)){
                
                foreach($data_all->linked->trndTrnsId as $vall ){

                    if($vall->trndType == 5 OR  $vall->trndType == 6 OR  $vall->trndType == 1){
                        
                        if($x % 2 == 0){
                             $style = "";
                        }else{
                             $style = "style='background-color:#ddd;'";
                        }
                       
                        $retry = 'onclick="window.location.href='."'"."update_items.php?cari=$cari&id=".$data_all->data->id."&detail=".$vall->id."'".'"';
                       

                        $toggle = "<label class='switch'>
                            <input $retry type='checkbox' >
                            <span class='slider round'></span>
                            </label>";
        
                        if(isset($data_all->data->trnsConfirmed) AND $data_all->data->trnsConfirmed == 1){
                            $toggle = "<b>lock</b>";
                        }

                        if($vall->trndType == 1){
                            $toggle = "<b>lock</b>";
                        }
                        
                        
                        echo "<tr $style>";
                        echo "<td><center>".$x."</center></td>";
                        
                        echo "<td>".$vall->trndDescription."</td>";
                        echo "<td align='right'>".number_format($vall->trndTotal,0,",",".")."</td>";
                        
                        echo "<td><center>".$toggle."</center></td>";	
                        echo "</tr>";
                     

                        $x++;
                    }
                
                }
	
            }
        
    
    ?>


    
</table>


