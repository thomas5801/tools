<?php 
include 'config.php';

    if(cek_session($url."api/v1/check") === false){
        header('Location: logout.php');
        exit;
    }

   
?>
<style>

.button {
  background-color: #4CAF50;
  border: none;
  color: white;
  padding: 5px 15px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 2px 1px;
  cursor: pointer;
}


.button1 {
  background-color: #c0bc03;
  border: 1;
  color: white;
  padding: 5px 15px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 2px 1px;
  cursor: pointer;
}


.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #4CAF50;
}

input:focus + .slider {
  box-shadow: 0 0 1px #4CAF50;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}


ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
  width:900;
}

li {
  float: left;
   width:75;
}



li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #ddd;
  color: black;
}

.active {
  background-color: #4CAF50;
  color: white;;
}

.block {
  display: block;
  width: 100%;
  border: true;
  background-color: #4CAF50;
  color: white;
  padding: 3px 60px;
  font-size: 12px;
  cursor: pointer;
  text-align: center;
}

.block:hover {
  background-color: #c0bc03;
  color: black;
}

</style>

<?php

$menu = str_replace("{{active_duplicate}}","active",$menu);
echo $menu;

?>

<br>

<table border="0" width='900'>
    <tr>
        <td> <h3>Duplicate BIB Event Participant</h3> </td>
    <tr>
    
</table>



<br>

<!--
<form action="system.php" method="get">
	<label>Cari :</label>
	<input type="text" name="cari">
	<input type="submit" value="Cari">
</form>
-->

<?php

   
    
    if(isset($_GET['cari'])){
        $cari = $_GET['cari'];
        echo "<b>Hasil pencarian : ".$cari."</b>";
    }

    if(isset($_GET['id'])){
        $idc = $_GET['id'];
    }

    if(isset($_GET['bib'])){
        $bib = $_GET['bib'];
     
    }

    

    //error_reporting(0);

    // GET DATA
    $ch = curl_init(); 
    
    if(isset($cari)){
        $url_ = $url."/api/v1/duplicatebib?filter[evnhName][like]=%25".urlencode($cari)."%25"; 
    }else{
        $url_ = $url."/api/v1/duplicatebib"; 
    }


	// set url
	curl_setopt($ch, CURLOPT_URL, $url_);

	// return the transfer as a string 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

	// $output contains the output string 
	$output = curl_exec($ch); 

	// tutup curl 
	curl_close($ch);      

	// menampilkan hasil curl
	echo " \n ";

	echo " \n ";
    $data_all = json_decode($output);
	
	
	// AMBIL DETAIL
	if(isset($bib) AND isset($idc)){
		$ch = curl_init(); 
		$url_ = $url."api/v1/resources/event_participant?filter[evpaBIBNo]=$bib&filter[evpaEvncId]=$idc";
		curl_setopt($ch, CURLOPT_URL, $url_);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);   
		$data_detail = json_decode($output);
		
		
		
		$ch = curl_init(); 
		$url_ = $url."api/v1/resources/event_participant?filter[evpaBIBNo][<>]=0&filter[evpaEvncId]=$idc&sort=-evpaBIBNo";
		curl_setopt($ch, CURLOPT_URL, $url_);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);   
		$rekbib = json_decode($output);
		

	}

?>




<table width='900' border="1">

    <tr>
        <th style='background-color:#c0bc03;' colspan="5" >================ DUPLICATE BIB PARTICIPANT ================</th>
    </tr>
    
	<tr>
        <th>Duplicate</th>
		<th>Event Name</th>
		<th>Category</th>
		<th>BIB</th>
		<th>Action</th>
    </tr>
    <?php 
        
            $x = 1;
            
            if(isset($data_all->data)){
                foreach($data_all->data as $vall ){
				
                    if($x % 2 == 0){
                        
                        $style = "";
                    }else{
                        
                        $style = "style='background-color:#ddd;'";
                    }
                   
                    $retry = 'onclick="window.location.href='."'".'duplicate.php?id='.$vall->evpaEvncId."&bib=".$vall->evpaBIBno."'".'"';
				   
                    if(isset($idc) AND isset($bib) AND $idc == $vall->evpaEvncId AND $bib == $vall->evpaBIBno ){
						$detail = true;
                        $dtl = "<b>Detail</b>";
                        $disabled = "disabled";
                    }else{
                        $disabled = "";
						$detail = false;
                        $dtl = "Detail";
                    }
                    
                    
                    echo "<tr $style>";
					echo "<td><center>".$vall->evpaDuplicate."</center></td>";
					echo "<td>".$vall->evnhName."</td>";
					echo "<td>".$vall->evncName."</td>";
					echo "<td><center><b>".$vall->evpaBIBno."<b></center></td>";
					echo "<td><button $retry class='button' $disabled>$dtl</button></td>";
                    echo "</tr>";
                 
				 
					if(isset($data_detail->data) AND $detail == true){
						
						$change = $rekbib->data[0]->evpaBIBNo+1;
						echo "<tr $style>";
						echo "<td colspan=5>";
						echo "<table width='900' border=0>";
						echo "<tr><th>Id</th><th>Name</th><th>BIB</th><th>CHANGE</th><th align=right>Action &nbsp;&nbsp;&nbsp;</th></tr>";
						foreach($data_detail->data as $valld ){
						
                        
                        $replace = 'onclick="window.location.href='."'".'update_participant_bib.php?id='.$vall->evpaEvncId."&bib=".$vall->evpaBIBno."&id_peserta=".$valld->evpaId."&bib_reg=".$change."'".'"';
						$replace = "<th align=right><button $replace class='button1'>Update</button></th>";
                        
						echo "<tr><th>$valld->evpaId</th><th>$valld->evpaName</th><th>$valld->evpaBIBNo</th><th>$change</th>$replace</tr>";
						
						}
						
						echo "</table>";
						echo "</td>";
						echo "</tr>";
					}
                    
                    
                    
                    $x++;
                
                }
      
			
            }
        
    // <tr>
    //    <td><b>Event</b></td>
    //    <td>:</td>
    //    <td>dsadas</td>
    // </tr>
    
    ?>


    
</table>


