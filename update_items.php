<?php
include 'config.php';

//GET /api/v1/removeitem/{TransaksiId}/{TransaksiDetailId}


 if(!isset($_GET['id'])){
    header('Location: remove.php?error[id]=empty');   
 }

 if(!isset($_GET['detail'])){
    header('Location: remove.php?error[detail]=empty');
  
 }



// GET DATA
$ch = curl_init(); 

$url_ = $url.'/api/v1/removeitem/'.$_GET['id']."/".$_GET['detail']; 
    
// set url
curl_setopt($ch, CURLOPT_URL, $url_);

// return the transfer as a string 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

// $output contains the output string 
$output = curl_exec($ch); 

// tutup curl 
curl_close($ch);      

// menampilkan hasil curl



$data_all = json_decode($output);




if(isset($data_all->status->error->message)){
	header('Location: remove.php?response_error='.$data_all->status->error->message);
}else{
	header('Location: remove.php?cari='.$_GET['cari']);
}


    

exit;