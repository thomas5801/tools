<?php 
include 'config.php';

    if(cek_session($url."api/v1/check") === false){
        header('Location: logout.php');
        exit;
    }
   
?>
<style>


.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #4CAF50;
}

input:focus + .slider {
  box-shadow: 0 0 1px #4CAF50;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
  width:900;
}

li {
  float: left;
   width:75;
}



li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #ddd;
  color: black;
}

.active {
  background-color: #4CAF50;
  color: white;;
}

.block {
  display: block;
  width: 100%;
  border: true;
  background-color: #4CAF50;
  color: white;
  padding: 3px 60px;
  font-size: 12px;
  cursor: pointer;
  text-align: center;
}

.block:hover {
  background-color: #c0bc03;
  color: black;
}

</style>





<?php

$menu = str_replace("{{active_user}}","active",$menu);
echo $menu;

?>

<br>

<table border="0" width='900'>
    <tr>
        <td> <h3>Form Tambah User</h3> </td>
    <tr>
    
</table>

<?php

if(isset($_GET['adduser_error'])  AND isset($_GET['response_adduser_error'])){
    
    echo "<font color='red'><b> ERROR ".$_GET['adduser_error']." : ". $_GET['response_adduser_error']."</b></font>";
    echo "<br>";
    echo "<br>";
}

if(isset($_GET['adduser'])){
    $adduser = $_GET['adduser'];
    echo "<font color='green'><b> DATA : ".$adduser."</b></font>";
    echo "<br>";
    echo "<br>";
    echo "<!-- ";
}

if(isset($_GET['admin'])){
    $admin_mode = $_GET['admin'];
    echo "<font color='green'><b> DATA : ".$admin_mode."</b></font>";
    echo "<br>";
    echo "<br>";
    echo "<!-- ";
}

if(isset($_GET['rpc'])){
    $rpc_mode = $_GET['rpc'];
    echo "<font color='green'><b> DATA : ".$rpc_mode."</b></font>";
    echo "<br>";
    echo "<br>";
    echo "<!-- ";
}


?>

<form action='add_user.php' method='post'>
    <table width='900' cellpadding='0' cellspacing='2' bordercolor='#666666'>
        <tr>
            <td width='100'><b>User Name</b></td>
            <td><input size="40" type="text" name='username' required></td>
        </tr>
		 <tr>
            <td width='100'><b>Password</b></td>
            <td><input size="40" type="text" name='password' required></td>
        </tr>
        <tr>
            <td width='100'><b>Full Name</b></td>
            <td><input size="60" type="text" name='fullname' required></td>
        </tr>
           <tr>
            <td width='100'><b>Email</b></td>
            <td><input size="60" type="email" name='email' required></td>
        </tr>
        
        </tr>
           <tr>
            <td ><b>Gender</b></td>
            <td>
                <select name="gender" required>
                    <option ></option>
                    <option value="1">Pria</option>
                    <option value="2">Wanita</option>
                </select>
            
            </td>
        </tr>
        
        <tr>
            <td ><b>Type</b></td>
            <td >  
                <select name="type" required>
                    <option ></option>
                    <option value="2">Race Director</option>
                    <option value="3">Event Coordinator</option>
                    <option value="4">Event Finance Coordinator</option>
                    <option value="5">Community Coordinator</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <b><input type='submit' value='Submit'></b>
            </td>
        </tr>
    </table>
</form>
<br>
<?php 

if(isset($_GET['adduser']) OR isset($_GET['admin']) OR isset($_GET['rpc']) ){
    echo " --> ";
}

?>






<form action="user.php" method="get">
	<label>Cari Berdasarkan <b>User Name</b> :</label>
	<input type="text" name="cari">
	<input type="submit" value="Cari">
</form>

<?php

    if(isset($_GET['cari'])){
        $cari = $_GET['cari'];
        echo "<b>Hasil pencarian : ".$cari."</b>";
    }


    //error_reporting(0);

    // GET DATA
    $ch = curl_init(); 
    
    if(isset($cari)){
        $url_ = $url."api/v1/resources/users/?filter[userName][like]=%25".urlencode($cari)."%25"; 
    }else{
        $url_ = $url."api/v1/resources/users/"; 
    }
    
    if(isset($adduser)){
       $url_ = $url."api/v1/resources/users/".$adduser; 
       $url_admin = $url."api/v1/resources/event_user?filter[evusUserId]=".$adduser;
       $url_rpc = $url."api/v1/resources/rpc_user?filter[reusUserId]=".$adduser;
    }
    
    if(isset($admin_mode)){
       $url_ = $url."api/v1/resources/users/".$admin_mode; 
       $url_admin = $url."api/v1/resources/event_user?filter[evusUserId]=".$admin_mode;
       //$url_rpc = $url."api/v1/resources/rpc_user?filter[reusUserId]=".$adduser;
    }
    
    if(isset($rpc_mode)){
       $url_ = $url."api/v1/resources/users/".$rpc_mode; 
       //$url_admin = $url."api/v1/resources/event_user?filter[evusUserId]=".$rpc_mode;
       $url_rpc = $url."api/v1/resources/rpc_user?filter[reusUserId]=".$rpc_mode;
    }

	// set url
	curl_setopt($ch, CURLOPT_URL, $url_);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    curl_setopt($ch, CURLOPT_COOKIE, 'PHPSESSID=' . $_COOKIE['PHPSESSID']); 
	$output = curl_exec($ch);
	curl_close($ch);      

	// menampilkan hasil curl
	echo " \n ";
	echo " \n ";
    $data_all = json_decode($output);
	


?>


<?php

// FORM INPUT USER ADMIN DAN RPC

    if(isset($admin_mode)){

        echo"<form action='add_admin_user.php?admin=$admin_mode' method='post'>";
            echo"<table width='900' cellpadding='0' cellspacing='2' bordercolor='#666666'>";
                echo"<tr>";
                    echo"<th colspan='2; width='900'><b>FORM USER ADMIN</b></th>";
                echo"</tr>";
                echo"<tr>";
                    echo"<td width='100'><b>Event Id</b></td>";
                    echo"<td><input type='number' min='1' name='event' required></td>";
                echo"</tr>";
                echo"<tr>";
                    echo"<td><b>Role Admin</b></td>";
                    echo"<td >
                    
                    
                    <select name='role' required>
                    <option ></option>
                    <option value='1'>Admin Event</option>
                    <option value='2'>Race Director</option>
                    <option value='3'>Race Coordinator</option>
                    <option value='4'>Event Finance Director</option>
                    <option value='5'>Community Coordinator</option>
                    </select>
                    
                    </td>";
                echo"</tr>";
                echo"<tr>";
                    echo"<td>&nbsp;</td>";
                    echo"<td>";
                        echo"<b><input type='submit' value='Submit'></b>";
                    echo"</td>";
                echo"</tr>";
            echo"</table>";
        echo"</form>";
        
    
    }
    
    
    if(isset($rpc_mode)){

        echo"<form action='add_rpc_user.php?rpc=$rpc_mode' method='post'>";
            echo"<table width='900' cellpadding='0' cellspacing='2' bordercolor='#666666'>";
                echo"<tr>";
                    echo"<th colspan='2; width='900'><b>FORM USER RACE PACK SYSTEM</b></th>";
                echo"</tr>";
                echo"<tr>";
                    echo"<td width='100'><b>Event Id</b></td>";
                    echo"<td><input type='number' min='1' name='event' required></td>";
                echo"</tr>";
                echo"<tr>";
                    echo"<td><b>Role RPC</b></td>";
                    echo"<td >
                    
                    
                    <select name='role' required>
                    <option ></option>
                    <option value='1'>CASHIR</option>
                    <option value='2'>CS</option>
                    <option value='3'>RPC</option>
                    <option value='4'>SUPER ADMIN</option>
                    <option value='5'>AGENT</option>
                    <option value='6'>KGMedia Merchant</option>
                    <option value='7'>KGMedia Kurir</option>
                    </select>
                    
                    </td>";
                echo"</tr>";
                echo"<tr>";
                    echo"<td>&nbsp;</td>";
                    echo"<td>";
                        echo"<b><input type='submit' value='Submit'></b>";
                    echo"</td>";
                echo"</tr>";
            echo"</table>";
        echo"</form>";
        
    
    }


?>


<table width='900' border="1">

    <tr>
        <th style='background-color:#c0bc03;' colspan="3" >=================== USER ===================</th>
    </tr>
    
    <?php 
        
            $x = 1;
            
            if(isset($data_all->data)){
                
                if(is_array($data_all->data)){
                    
                    foreach($data_all->data as $vall ){
                    
                        if($x % 2 == 0){
                             $style = "";
                        }else{
                             $style = "style='background-color:#ddd;'";
                        }
                       
                        
                        echo "<tr $style>";
                        echo "<td><b>Id</b></td>";
                        echo "<td>:</td>";
                        echo "<td>".$vall->id."</td>";
                        echo "</tr>";
                        
                        echo "<tr $style>";
                        echo "<td><b>User Name</b></td>";
                        echo "<td>:</td>";
                        echo "<td>".$vall->userName." </td>";
                        echo "</tr>";
                        
                        echo "<tr $style>";
                        echo "<td><b>Full Name</b></td>";
                        echo "<td>:</td>";
                        echo "<td>".$vall->userFullName." </td>";
                        echo "</tr>";
                        
                        
                        echo "<tr $style>";
                        echo "<td><b>Email</b></td>";
                        echo "<td>:</td>";
                        echo "<td>".$vall->userEmail." </td>";
                        echo "</tr>";
                        
                        
                        
                        if($vall->userGender == '1'){
                            $vall->userGender ="Pria";
                        }else{
                            $vall->userGender ="Wanita";
                        }
                        
                        echo "<tr $style>";
                        echo "<td><b>Gender</b></td>";
                        echo "<td>:</td>";
                        echo "<td>".$vall->userGender." </td>";
                        echo "</tr>";
                        
                        
                       
                        
                        if($vall->userType == '1'){
                            $vall->userType = "Administrator";
                        }else if($vall->userType == '2'){
                            $vall->userType ="Race Director";
                        }else if($vall->userType == '3'){
                            $vall->userType ="Event Coordinator";
                        }else if($vall->userType == '4'){
                            $vall->userType ="Event Finance Coordinator";
                        }else if($vall->userType == '5'){
                            $vall->userType ="Community Coordinator";
                        }else{
                            $vall->userType ="Undefine";
                        }
                        
                        echo "<tr $style>";
                        echo "<td><b>Type</b></td>";
                        echo "<td>:</td>";
                        echo "<td>".$vall->userType." </td>";
                        echo "</tr>";
                        
                        
                        echo "<tr $style>";
                        echo "<td><b>Created Time</b></td>";
                        echo "<td>:</td>";
                        echo "<td>".$vall->userCreatedTime." </td>";
                        echo "</tr>";
                        
                        
                        $detail = 'onclick="window.location.href='."'".'user.php?adduser='.$vall->id."'".'"';
                        $rpc = 'onclick="window.location.href='."'".'user.php?rpc='.$vall->id."'".'"';
                        $admin = 'onclick="window.location.href='."'".'user.php?admin='.$vall->id."'".'"';
                        
                        $table_user ="
                        <table width=100%>
                        <tr>
                        <td><button $detail class='block'>Detail  </button></td>
                        <td><button $rpc class='block'>(+) RPC  </button></td>
                        <td><button $admin class='block'>(+) Admin  </button></td>

                        </tr>
                        </table>";
                        
                        
                        echo "<tr $style>";
                        echo "<td colspan='3'> $table_user </td>";
                        echo "</tr>";
                        
                        echo "<tr >";
                        echo "<td colspan='3'>&nbsp;</td>";
                        echo "</tr>";
                        
                        $x++;
                    
                    }
                }else{
                    
                        if($x % 2 == 0){
                             $style = "";
                        }else{
                             $style = "style='background-color:#ddd;'";
                        }
                       
                        
                        echo "<tr $style>";
                        echo "<td><b>Id</b></td>";
                        echo "<td>:</td>";
                        echo "<td>".$data_all->data->id."</td>";
                        echo "</tr>";
                        
                        echo "<tr $style>";
                        echo "<td><b>User Name</b></td>";
                        echo "<td>:</td>";
                        echo "<td>".$data_all->data->userName." </td>";
                        echo "</tr>";
                        
                        echo "<tr $style>";
                        echo "<td><b>Full Name</b></td>";
                        echo "<td>:</td>";
                        echo "<td>".$data_all->data->userFullName." </td>";
                        echo "</tr>";
                        
                        
                        echo "<tr $style>";
                        echo "<td><b>Email</b></td>";
                        echo "<td>:</td>";
                        echo "<td>".$data_all->data->userEmail." </td>";
                        echo "</tr>";
                        
                        
                        
                        if($data_all->data->userGender == '1'){
                            $data_all->data->userGender ="Pria";
                        }else{
                            $data_all->data->userGender ="Wanita";
                        }
                        
                        echo "<tr $style>";
                        echo "<td><b>Gender</b></td>";
                        echo "<td>:</td>";
                        echo "<td>".$data_all->data->userGender." </td>";
                        echo "</tr>";
                        
                        
                       
                        
                        if($data_all->data->userType == '1'){
                            $data_all->data->userType = "Administrator";
                        }else if($data_all->data->userType == '2'){
                            $data_all->data->userType ="Race Director";
                        }else if($data_all->data->userType == '3'){
                            $data_all->data->userType ="Event Coordinator";
                        }else if($data_all->data->userType == '4'){
                            $data_all->data->userType ="Event Finance Coordinator";
                        }else if($data_all->data->userType == '5'){
                            $data_all->data->userType ="Community Coordinator";
                        }else{
                            $data_all->data->userType ="Undefine";
                        }
                        
                        echo "<tr $style>";
                        echo "<td><b>Type</b></td>";
                        echo "<td>:</td>";
                        echo "<td>".$data_all->data->userType." </td>";
                        echo "</tr>";
                        
                        
                        echo "<tr $style>";
                        echo "<td><b>Created Time</b></td>";
                        echo "<td>:</td>";
                        echo "<td>".$data_all->data->userCreatedTime." </td>";
                        echo "</tr>";
                        
                        
                        $detail  = 'onclick="window.location.href='."'".'user.php?detail='.$data_all->data->id."'".'"';
                        $rpc     = 'onclick="window.location.href='."'".'user.php?rpc='.$data_all->data->id."'".'"';
                        $admin   = 'onclick="window.location.href='."'".'user.php?admin='.$data_all->data->id."'".'"';
                        
                        
                        // <td><button $detail class='block'>Detail  </button></td>
                        $table_user ="
                        <table width=100%>
                        <tr>
                        
                        <td><button $rpc class='block'>(+) RPC  </button></td>
                        <td><button $admin class='block'>(+) Admin  </button></td>

                        </tr>
                        </table>";
                        
                        
                        echo "<tr $style>";
                        echo "<td colspan='3'> $table_user </td>";
                        echo "</tr>";
                        
                        echo "<tr >";
                        echo "<td colspan='3'>&nbsp;</td>";
                        echo "</tr>";
                        
                        $x++;
                    
                }
      
			
            }
        
    // <tr>
    //    <td><b>Event</b></td>
    //    <td>:</td>
    //    <td>dsadas</td>
    // </tr>
    
    ?>


    
</table>






<?php


    if(isset($url_admin)){ // admin 
	

        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url_admin.'&pageSize=150');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_COOKIE, 'PHPSESSID=' . $_COOKIE['PHPSESSID']); 
        $output = curl_exec($ch);
        curl_close($ch);      

        echo"<br>";
        echo"<br>";
        // menampilkan hasil curl
        echo " \n ";
        echo " \n ";
        $data_all_admin = json_decode($output);

   
        echo"<table width='900' border='1'>";

            echo"<tr>";
                echo"<th style='background-color:#c0bc03;' colspan='5' >=================== ADMIN ===================</th>";
            echo"</tr>";
            
            echo"<tr>";
            echo" <th>Event Id</th>";
            echo" <th>Event Name</th>";
            echo" <th>Role</th>";
            echo" <th>Status</th>";
            echo" <th>Action</th>";
            echo"</tr>";
            
            
            $master_event_user_role = array(
                1=>"Admin Event",
                2=>"Race Director",
                3=>"Race Coordinator",
                4=>"Event Finance Director",
                5=>"Community Coordinator",
                
            
            );
            
            if(isset($data_all_admin->data)){
                foreach($data_all_admin->data as $vall ){
					
		
                    if($x % 2 == 0){
                         $style = "";
                    }else{
                         $style = "style='background-color:#ddd;'";
                    }
					
				
				   
				    if($vall->evusDeletedTime == null){

						$_param = '?admin='.$vall->evusUserId.'&admin_action='.$vall->evusId."&status=1";
						$active = 'onclick="window.location.href='."'".'inactive_admin_user.php'."$_param'".'"';
						
                        $toggle = "<label class='switch'>
						<input $active type='checkbox' checked>
						<span class='slider round'></span>
						</label>";
						
						$vall->evusDeletedTime = "Active";
                    }else{
						
						$_param = '?admin='.$vall->evusUserId.'&admin_action='.$vall->evusId."&status=0";
						$active = 'onclick="window.location.href='."'".'inactive_admin_user.php'."$_param'".'"';
                        $toggle = "<label class='switch'>
						<input $active type='checkbox' >
						<span class='slider round'></span>
						</label>";
						$vall->evusDeletedTime = "Inactive";
                    }
                    
                    echo "<tr $style>";
                        echo "<td><center>".$vall->links->evusEvnhId."</center></td>";
                         
                        foreach($data_all_admin->linked->evusEvnhId as $vall_event){
                            if($vall_event->id == $vall->links->evusEvnhId){
                                $vall->links->evusEvnhId = $vall_event->evnhName;
                            }
                            
                        }
                        
                        $vall->evusEvurId = $master_event_user_role[$vall->evusEvurId];
                        
                        echo "<td>".$vall->links->evusEvnhId."</td>";
                        echo "<td>".$vall->evusEvurId."</td>";
                        echo "<td><b>".$vall->evusDeletedTime."<b></td>";
                        echo "<td><center>".$toggle."</center></td>";
                    //echo "</tr>";
						
						
                    //echo "</tr>";
					//echo "<tr $style>";
					//echo "<td colspan='5'>&nbsp;</td>";
                    //echo "</tr>";
                    
                    
                    
                    $x++;
                
                
                
                }
            }

        echo"</table>";

    }
    
    
    if(isset($url_rpc)){
        
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url_rpc);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_COOKIE, 'PHPSESSID=' . $_COOKIE['PHPSESSID']); 
        $output = curl_exec($ch);
        curl_close($ch);      

        // menampilkan hasil curl
        echo " \n ";
        echo " \n ";
        $data_all_admin = json_decode($output);

        echo"<br>";
        echo"<br>";
        echo"<table width='900' border='1'>";

            echo"<tr>";
                echo"<th style='background-color:#c0bc03;' colspan='5' >=================== RPC ===================</th>";
            echo"</tr>";
            
            echo"<tr>";
            echo" <th>Event Id</th>";
            echo" <th>Event Name</th>";
            echo" <th>Role</th>";
            echo" <th>Status</th>";
            echo" <th>Action</th>";
            echo"</tr>";
            
            
            $master_user_rpc_role = array(
                1=>"CASHIR",
                2=>"CS",
                3=>"RPC",
                4=>"SUPER ADMIN",
                5=>"AGENT",
                6=>"GMedia Merchant",
                7=>"KGMedia Kurir",
                
                 
                
            
            );
            
            if(isset($data_all_admin->data)){
                foreach($data_all_admin->data as $vall ){
					
                    if($x % 2 == 0){
                         $style = "";
                    }else{
                         $style = "style='background-color:#ddd;'";
                    }
            
            
                    //$active = 'onclick="window.location.href='."'".'system.php'."'".'"';
                    $active = "";
				   
				    if($vall->reusDeletedTime == null){
                        $toggle = "<label class='switch'>
						<input $active type='checkbox' checked>
						<span class='slider round'></span>
						</label>";
						
						$vall->reusDeletedTime = "Active";
                    }else{
                        $toggle = "<label class='switch'>
						<input $active type='checkbox' >
						<span class='slider round'></span>
						</label>";
						$vall->reusDeletedTime = "Inactive";
                    }
                    
                    echo "<tr $style>";
                        echo "<td><center>".$vall->links->reusEvnhId."</center></td>";
                         
                        foreach($data_all_admin->linked->reusEvnhId as $vall_event){
                            if($vall_event->id == $vall->links->reusEvnhId){
                                $vall->links->reusEvnhId = $vall_event->evnhName;
                            }
                            
                        }
                        
                        $vall->reusRevrId = $master_user_rpc_role[$vall->reusRevrId];
                        
                        echo "<td>".$vall->links->reusEvnhId."</td>";
                        echo "<td>".$vall->reusRevrId."</td>";
                        echo "<td><b>".$vall->reusDeletedTime."<b></td>";
                        echo "<td><center>".$toggle."</center></td>";
                    //echo "</tr>";
						
						
                    //echo "</tr>";
					//echo "<tr $style>";
					//echo "<td colspan='5'>&nbsp;</td>";
                    //echo "</tr>";
                    
                    
                    
                    $x++;
                
                
                
                }
            }

        echo"</table>";

    }
    
    
?>

