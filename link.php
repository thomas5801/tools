<?php 
include 'config.php';

    if(cek_session($url."api/v1/check") === false){
        header('Location: logout.php');
        exit;
    }


    // ADD
    if(isset($_POST['event'])  AND isset($_POST['url'])){
        
        $event_link = $_POST['event'];
        $url_link = $_POST['url'];
        

        $hashed_string['linkEvnhId'] = $event_link;
        $hashed_string['linkUrl'] = $url_link;
        
        $data_post = array(
            'data' => $hashed_string,
        );
        
        $response = get_content($url.'/api/v1/directlink/', json_encode($data_post));
        
        unset($_POST['event']);
        unset($_POST['url']);
        

        $response = json_decode($response);

       if(isset($response->status->error->message)){
           echo "<font color='red'><b> ERROR : ". $response->status->error->message."</b></font>";
           
           echo "<br>";
           echo "<br>";
       }else{
           header('Location: ' ."?cari_aja=".$response->data->id);
       }
    }
	
	

	if(isset($_POST['id_update'])  AND isset($_POST['url_update'])){
        
        $id_update = $_POST['id_update'];
        $url_update = $_POST['url_update'];
        

        $hashed_string['linkUrl'] = $url_update;
        
        $data_post = array(
            'data' => $hashed_string,
        );
        
        $response = get_content($url.'/api/v1/update_directlink/'.$id_update, json_encode($data_post));
        
        unset($_POST['id_update']);
        unset($_POST['url_update']);
        

        $response = json_decode($response);

       if(isset($response->status->error->message)){
           echo "<font color='red'><b> ERROR : ". $response->status->error->message."</b></font>";
           
           echo "<br>";
           echo "<br>";
       }else{
           header('Location: ' ."?cari_aja=".$response->data->id);
       }
    }

   
?>
<style>


.back {
  display: block;
  width: 100%;
  border: true;
  background-color: #4CAF50;
  color: white;
  padding: 10px 60px;
  font-size: 12px;
  cursor: pointer;
  text-align: center;
}

.block {
  display: block;
  width: 100%;
  border: true;
  background-color: #4CAF50;
  color: white;
  padding: 3px 60px;
  font-size: 12px;
  cursor: pointer;
  text-align: center;
}

.block:hover {
  background-color: #c0bc03;
  color: black;
}


ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
  width:900;
}

li {
  float: left;
   width:75;
}



li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #ddd;
  color: black;
}

.active {
  background-color: #4CAF50;
  color: white;;
}


</style>


<?php

$menu = str_replace("{{active_link}}","active",$menu);
echo $menu;

?>

<br>

<table border="0" width='900'>
    <tr>
        <td> <h3>Form Tambah Direct Link  </h3> </td>

    <tr>
    
</table>


<?php

    if(isset($_GET['cari'])){
        $cari = $_GET['cari'];
    }
	if(isset($_GET['cari_aja'])){
        $cari_aja = $_GET['cari_aja'];
    }

    //error_reporting(0);

    // GET DATA
    $ch = curl_init(); 
    
    if(isset($cari)){
        $url_ = $url."api/v1/resources/direct_link/".$cari; 
    }else{
        $url_ = $url."api/v1/resources/direct_link/"; 
    }
	
	if(isset($cari_aja)){
		if(empty($cari_aja)){
			$url_ = $url."api/v1/resources/direct_link/"; 
		}else{
			$url_ = $url."api/v1/resources/direct_link/?filter[linkEvnhId]=".$cari_aja; 
		}
        
    }


	// set url
	curl_setopt($ch, CURLOPT_URL, $url_);

	// return the transfer as a string 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

	// $output contains the output string 
	$output = curl_exec($ch); 

	// tutup curl 
	curl_close($ch);      

	// menampilkan hasil curl
	echo " \n ";

	echo " \n ";
    $data_all = json_decode($output);
	
	
	if(!empty($cari)){
		 if(isset($data_all->data)){
			$update_id    = $data_all->data->id;
			$update_event =	$data_all->linked->linkEvnhId[0]->evnhName;

			$update_url = $data_all->data->linkUrl;
			
			echo "<form action='link.php' method='post'>
				
				<table width='900' cellpadding='0' cellspacing='2' bordercolor='#666666'>
					<tr>
						<th  colspan='2'>Update Direct Link</th>
					</tr>
					<tr>
						<th  colspan='2'>&nbsp;</th>
					</tr>
					<tr>
						<td width='100'><b>&nbsp;&nbsp;</b></td>
						<td><input  value ='$update_id' type='number' min='1' name='id_update' required hidden></td>
					</tr>
					<tr>
						<td width='100'><b>&nbsp;&nbsp;Event Name</b></td>
						<td><input size='60' value ='$update_event'  type='text' name='event' required disabled></td>
					</tr>
					<tr>
						<td valign=top><b>&nbsp;&nbsp;Url Direct</b></td>
						<td ><input value ='$update_url' size='60' type='url' name='url_update' rows=4 cols=40 required></textarea></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>
							<b><input type='submit' value='Update'></b>
						</td>
					</tr>
					<tr>
						<th  colspan='2'>&nbsp;</th>
					</tr>
				</table>
			</form>";

			
		 }else{
			header('Location: link.php?error= Yang di cari ga ada');
			exit;
		 }
	 
	}else{
		
		echo "<form action='link.php' method='post'>
			<table width='900' cellpadding='0' cellspacing='2' bordercolor='#666666'>
				<tr>
					<td width='100'><b>Event Id</b></td>
					<td><input  type='number' min='1' name='event' required ></td>
				</tr>
				<tr>
					<td valign=top><b>Url Direct</b></td>
					<td ><input  size='60' type='url' name='url' rows=4 cols=40 required></textarea></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<b><input type='submit' value='Submit'></b>
					</td>
				</tr>
			</table>
		</form>";
		
	}
	
	
	

?>



<br>

<table width='900'>
<tr>
<td>
<?php
if(isset($_GET['cari'])){
	$back = 'onclick="window.location.href='."'".'link.php'."'".'"';
	echo "<form><button $back class ='block'>Back</button> </form>";
}


?>

</td>
<td align="right" valign="middle">
<form action="link.php" method="get">
	<label>Cari Berdasarkan <b>Id Event</b> : </label>
	<input type="text" name="cari_aja">
	<input type="submit" value="Cari">
</form>
</td>
</tr>

</table>

<?php

    if(isset($cari_aja)){
		echo "<b>Hasil pencarian : ".$cari_aja."</b>";   
    }
	
	if(isset($_GET['error'])){
		$error = urldecode($_GET['error']);
        echo "<b>Hasil pencarian : ".$error."</b><br><br>";   
    }
	
if(!isset($cari)){
		echo "<table width='900' border='1'>";
		echo "<tr><th style='background-color:#c0bc03;' colspan='3' >=================== LINK ===================</th></tr>";
            

		if(isset($data_all->data)){
		
			$x = 1;
            foreach($data_all->data as $vall ){
            
                if($x % 2 == 0){
                     $style = "";
                }else{
                     $style = "style='background-color:#ddd;'";
                }
               
                
                echo "<tr $style>";
                echo "<td><b>Id</b></td>";
                echo "<td>:</td>";
                echo "<td>".$vall->id."</td>";
                echo "</tr>";
                
                echo "<tr $style>";
                echo "<td><b>Event</b></td>";
                echo "<td>:</td>";
                echo "<td>".$vall->links->linkEvnhId." </td>";
                echo "</tr>";
                
                
                
                foreach($data_all->linked->linkEvnhId as $vall_event ){
                    if($vall_event->id == $vall->links->linkEvnhId){
                        $vall->links->linkEvnhId = $vall_event->evnhName;
                        break;
                    }
                    
                
                }
                echo "<tr $style>";
                echo "<td><b>Event Name</b></td>";
                echo "<td>:</td>";
                echo "<td>".$vall->links->linkEvnhId." </td>";
                echo "</tr>";
                
                echo "<tr $style>";
                echo "<td><b>URL</b></td>";
                echo "<td>:</td>";
                echo "<td>".$vall->linkUrl." </td>";
                echo "</tr>";
				
				
				$update  = 'onclick="window.location.href='."'".'link.php?cari='.$vall->id."'".'"';
				$delete	 = 'onclick="window.location.href='."'".'link.php?delete='.$vall->id."'".'"';
				
				
				
				$table_user ="
                        <table width=100%>
                        <tr>
                        
                        <td><button $update class='block'> Update  </button></td>
                        <td><button $delete class='block'> Delete (*belum bisa)</button></td>

                        </tr>
                        </table>";
						
				echo "<tr $style>";
				echo "<td colspan='3'> $table_user </td>";
				echo "</tr>";
									
                echo "<tr >";
                echo "<td colspan='3'>&nbsp;</td>";
                echo "</tr>";
                
                $x++;
            
            }
      
        
		}
		echo '</table>';

	}

?>